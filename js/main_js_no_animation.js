xH = new XMLHttpRequest();

xVars = {};
lastModified_old = '';
source_file = 'StreamControl_0_4b/streamcontrol.xml';

// Initialize + set a Timeout function
function init() {
	loadXML;
	var timeout = this.window.setInterval(function() {
		loadXML();
	}, 1000);
}

// Load + Process the XML data 
function loadXML() { 
	xH.overrideMimeType('text/xml');
	
	xH.open('GET', source_file, true);
	xH.send();
	xH.onreadystatechange = function() {
		if (xH.readyState == 4) {
			// Check the Modified Timestamp
			if ((lastModified = xH.responseXML.lastModified) != lastModified_old) { 
				lastModified_old = lastModified;
				storeXML(xH.responseXML.childNodes[1].children);
				updateContent();
			}
		}
	}
	roundTimer();
	commentary();
}

// Store the XML data to local variables
function storeXML(xmlSource) { 
	for (var N in xmlSource) {
		if (xmlSource[N].nodeName != undefined) { 
			xVars[xmlSource[N].nodeName] = xmlSource[N].textContent; 
		}
	}
}

// Set page content to match updated XML data.
function updateContent() {
	for (var key in xVars) { 
		//console.log(key + ": " + xVars[key]);
		if (key == 'enable_poison') {
			if (xVars[key] == '0') {
				doAnimation('.poison', '', 'easeOut', '0');
			}
			if (xVars[key] == '1') {
				doAnimation('.poison', '', 'easeIn', '0');
			}
		} else if ($('#' + key).length && $('#' + key).html() != xVars[key]) {
			// animationType='ease_0'
			//aStyle = $('#' + key).attr('animationType').split('_');
			//doAnimation('#' + key, xVars[key], aStyle[0], aStyle[1]);
			$('#' + key).html(xVars[key]);
		}
	}
}

// Animations
function doAnimation(key, newData, animation, distance) {	
	distance = parseFloat(distance);
	switch(animation){
		case 'ease': 
			$(key).tween({
				opacity: {
					start: 100,
					stop: 0,
					time: 0,
					duration: 0.5,
					effect: 'easeOut'
				}
			});
			$(key).tween({
				opacity: {
					start: 0,
					stop: 100,
					time: 1,
					duration: 0.5,
					effect: 'easeIn'
				},
				onStart: function() {
					$(key).html(newData);
				}
			});
			break;
		case 'easeIn':
			console.log('easeIn called for ' + key);
			$(key).tween({
				opacity: {
					stop: 100,
					time: distance,
					duration: 0.8,
					effect: 'easeInOut'
				}
			});
			break;
		case 'easeOut':
			$(key).tween({
				opacity: {
					stop: 0,
					time: distance,
					duration: 0.8,
					effect: 'easeInOut'
				}
			});
			break;
		case 'slideUp':
			$(key).tween({
				top: {
					start: '0',
					stop: '-' + distance,
					time: 0,
					duration: 1.5,
					effect: 'easeOut'
				}
			});
			break;
		case 'slideDown':
			$(key).tween({
				top: {
					start: distance,
					stop: '0',
					time: 0,
					duration: 1.5,
					effect: 'easeOut'
				}
			});
			break;
		case 'slideLeft':
			$(key).tween({
				left: {
					start: '0',
					stop: '-' + distance,
					time: 0,
					duration: 1.5,
					effect: 'easeOut'
				}
			});
			break;
		case 'slideRight': 
			$(key).tween({
				left: {
					start: '0',
					stop: distance,
					time: 0,
					duration: 1.5,
					effect: 'easeOut'
				}
			});
			break;
	}
	$.play();
}

// Manage the round timer.
function roundTimer() {
	round_end = xVars['event_round_ends'].split(':');
	
	endTime = new Date()
	endTime.setHours(parseInt(round_end[0]));
	endTime.setMinutes(parseInt(round_end[1]));
	endTime.setSeconds(0);
	
	var currentTime = new Date();
	
	var timeRemaining = endTime.valueOf() - currentTime.valueOf(); 
	
	var secsL = timeRemaining / 1000;
	var minutesL = secsL / 60;
	var hoursL = minutesL / 60;
	
	secsL = parseInt(secsL % 60);
	minutesL = parseInt(minutesL % 60);
	hoursL = parseInt(hoursL % 24);
	
	if (timeRemaining > 0) {
		time_left = ((hoursL > 0) ? leadingZero(hoursL) + ':' : '') + leadingZero(minutesL) + ":" + leadingZero(secsL);
	} else { 
		time_left = "00:00";
	}
	
	$('#event_round_timer').html(time_left);
}

function leadingZero(num) { 
	return num < 10 ? "0" + num : num; 
}

function commentary() {
	comm_left = xVars['event_comm_name_left'];
	comm_right = xVars['event_comm_name_right'];
	
	if (comm_left != '' && comm_right != '') {
		str = comm_left + ' & ' + comm_right;
	} else {
		str = comm_left + comm_right;
	}
	//console.log($('#event_comm')[0].textContent);
	if ($('#event_comm')[0].textContent != str) {
		$('#event_comm').html(str);
		//doAnimation('#event_comm', str, 'ease', '0');
	}
}